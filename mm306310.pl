use_module(library(lists)).

clean_grammar([prod(FirstT,Prods) | GrammarDirty], Grammar) :-
    merge_duplicate_rules([prod('Z', [[nt(FirstT), '#']]), prod(FirstT, Prods) | GrammarDirty], Grammar).

merge_duplicate_rules(GrammarDirty, Grammar) :-
   extract_terms_and_nonterms(GrammarDirty, _, Nonterms),
   build_rules(GrammarDirty, Nonterms,[], Grammar).

build_rules(GrammarDirty, [E | Nonterms], Visited, Grammar) :-
    (member(E, Visited) ->
        build_rules(GrammarDirty, Nonterms, Visited, Grammar);
        get_rules_for_nonterm(GrammarDirty, E, Rules),
        build_rules(GrammarDirty, Nonterms, [E | Visited], GrammarT),
        Grammar = [prod(E, Rules) | GrammarT]).
build_rules(_, [], _, []).

get_rules_for_nonterm([prod(Nonterm, RulesH)|Grammar], Nonterm, Rules) :-
    get_rules_for_nonterm(Grammar, Nonterm, RulesR),
    union(RulesH, RulesR, Rules), !.
get_rules_for_nonterm([_|Grammar], Nonterm, Rules) :-
    get_rules_for_nonterm(Grammar, Nonterm, Rules).
get_rules_for_nonterm([], _, []).

firsts(Grammar, List, FirstSet) :-
    first_set_for_prod(Grammar, List, [], [], FirstSet).

first_set(Grammar, E, FirstSet) :- 
    first_set_h(Grammar, E, [], [], FirstSetNonUnique),
    remove_duplicates(FirstSetNonUnique, FirstSet).

first_set_h(Grammar, E, Visited, Acu, FirstSet) :-
    (E = nt(Nt) ->
        (member(Nt, Visited) ->
            FirstSet = [];
            get_rules_for_nonterm(Grammar, Nt, Rules),
            first_set_for_rules(Grammar, Rules, [Nt | Visited], Acu, FirstSet));
        FirstSet = [E]).

first_set_for_rules(Grammar, [Prods | Rules], Visited, Acu, FirstSet) :-
    first_set_for_prod(Grammar, Prods, Visited, [], Result),
    append(Result, Acu, FirstSetH),
    first_set_for_rules(Grammar, Rules, Visited, FirstSetH, FirstSet).
first_set_for_rules(_, [],  _, FirstSet, FirstSet).
    
first_set_for_prod(Grammar, [First | Prods], Visited, Acu, FirstSet) :-
    first_set_h(Grammar, First, Visited, Acu, FirstSetOfE),
    (member(epsilon, FirstSetOfE) ->
        delete(FirstSetOfE, epsilon, FirstSetH),
        first_set_for_prod(Grammar, Prods, Visited, FirstSetH, FirstSet);
        FirstSet = FirstSetOfE).
first_set_for_prod(_, [], _, FirstSet, [epsilon | FirstSet]).

follow_set(Grammar, E, FS) :-
    follow(Grammar, FSs),
    member((E, FS), FSs).

follow(DirtyGrammar, FollowSets) :-
    clean_grammar(DirtyGrammar, Grammar),
    extract_terms_and_nonterms(Grammar, _, Nonterms),
    create_follow_to_populate(Nonterms, FSE),
    follow_h(Grammar, FSE, FollowSets).

create_follow_to_populate([], []).
create_follow_to_populate([NT | NTs], [(NT, []) | R]) :- create_follow_to_populate(NTs, R).

follow_h(Grammar, Acu, FS) :-
    follow_for_each_rule(Grammar, Grammar, Acu, FS).
follow_for_each_rule(Grammar, [prod(NT, Prods) | GrammarR], Acu, FS) :-
    follow_for_each_prod(Grammar, NT, Prods, Acu, FSH),
    follow_for_each_rule(Grammar, GrammarR, FSH, FS).
follow_for_each_rule(_Grammar, [], FS, FS).

follow_for_each_prod(Grammar, _NT, [Prod | _Prods], Acu, FS) :-
    append(_, [nt(B)| C], Prod),
    C \= [],
    firsts(Grammar, C, CFR),
    delete(CFR, epsilon, CF),
    append(FSH, [(B, F)| FST], Acu),
    union(F, CF, FR),
    \+ samesets(F, FR),
    append(FSH, [(B, FR) | FST], NAcu),
    follow_h(Grammar, NAcu, FS),!.
follow_for_each_prod(Grammar, NT, [Prod | _Prods], Acu, FS) :-
    append(_, [nt(B)| C], Prod),
    firsts(Grammar, C, CFR),
    member(epsilon, CFR),
    append(FSH, [(B, BF) | FST], Acu),
    append(_, [(NT, NTF) | _], Acu),
    union(BF, NTF, NBF),
    \+ samesets(BF, NBF),
    append(FSH, [(B, NBF) | FST], NAcu),
    follow_h(Grammar, NAcu, FS),!.
follow_for_each_prod(Grammar, NT, [_ | Prods], Acu, FS) :-
    follow_for_each_prod(Grammar, NT, Prods, Acu, FS).
follow_for_each_prod(_, _, [], FS, FS).

extract_terms_and_nonterms(Grammar, Terms, Nonterms) :-
    extract_terms_and_nonterms_h(Grammar, ATerms, ANonterms),
    remove_duplicates(ATerms, Terms),
    remove_duplicates(ANonterms, Nonterms).
    
extract_terms_and_nonterms_h([prod(Nonterm, RuleList) | Grammar], Terms, [Nonterm | Nonterms]) :-
    flatten(RuleList, FlattenList),
    extract_terms_and_nonterms_from_flatten_list(FlattenList, TermsH, _),
    extract_terms_and_nonterms_h(Grammar, TermsT, Nonterms),
    append(TermsH, TermsT, Terms).
extract_terms_and_nonterms_h([], [], []).

extract_terms_and_nonterms_from_flatten_list([E|List], Terms, Nonterms) :-
    (E = nt(Nt) ->
        extract_terms_and_nonterms_from_flatten_list(List, Terms, NontermsT),
        Nonterms = [Nt|NontermsT];
        extract_terms_and_nonterms_from_flatten_list(List, TermsT, Nonterms),
        Terms = [E|TermsT]).
extract_terms_and_nonterms_from_flatten_list([], [], []).

remove_duplicates(List, Pruned) :-
    list_to_set(List, Pruned).

samesets(A,B) :- subset(A,B), subset(B,A).


%%%%%%%%%%%%%%%%%%%%%%%%%%%% AUTOMAT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

closure(Grammar, prod(NT, [P]), Closure) :-
    closure_h(Grammar, [dot_prod(NT, [], P)], Closure).
    
closure_h(Grammar, Prods, Closure) :-
    member(dot_prod(_, _, [nt(E) | _]), Prods),
    get_rules_for_nonterm(Grammar, E, Rules),
    change_to_dot_prod(E, Rules, EProds),
    union(EProds, Prods, Joined),
    \+ samesets(Prods, Joined), !,
    closure_h(Grammar, Joined, Closure).
closure_h(_, C, C).

go_to_single(Grammar, Prods, E, Result) :-
    go_to(Grammar, Prods, E, AResult),
    closure_h(Grammar, AResult, Result).

go_to(Grammar, Prods, E, Result) :-
    go_to_h(Prods, E, ARes),
    closure_h(Grammar, ARes, Result).

go_to_h([dot_prod(N, L, [E|R]) | Prods], E, [dot_prod(N, LE, R) | Result]) :-
    append(L, [E], LE),
    go_to_h(Prods, E, Result), !.
go_to_h([_ | Prods], E, Result) :- go_to_h(Prods, E, Result).
go_to_h([], _, []).
    
change_to_dot_prod(Nt, [Prod | Rules], [dot_prod(Nt, [], Prod) | DotProds]) :-
    change_to_dot_prod(Nt, Rules, DotProds).
change_to_dot_prod(_, [], []).
    
createSLR1(GrammarDirty, Machine, Info) :-
    clean_grammar(GrammarDirty, Grammar),
    extract_terms_and_nonterms(Grammar, Terms, NontermsH),
    add_nt_to_nonterms(NontermsH, Nonterms),
    append(Terms, Nonterms, AAll),
    delete(AAll, '#', All),
    [FirstElem | _] = Grammar,
    closure(Grammar, FirstElem, StartSet),
    construct_parse_table(Grammar, All, [StartSet], [], M, ok, Info),
    Machine = (StartSet, M).


add_nt_to_nonterms([E | Nonterms], [nt(E) | NontermsWithNt]) :- add_nt_to_nonterms(Nonterms, NontermsWithNt).
add_nt_to_nonterms([], []).

set_member((Set, _), [(H, _) | _]) :- samesets(Set, H), !.
set_member(S, [_ | T]) :- set_member(S, T).

construct_parse_table(_, _, _, _, null, Status, Status) :- Status \= ok, !.
construct_parse_table(Grammar, Items, [STD | Rest], AM, M, Status, Info) :-
    set_member((STD, _), AM), !,
    construct_parse_table(Grammar, Items, Rest, AM, M, Status, Info).
construct_parse_table(Grammar, Items, [STD | Rest], AM, M, Status, Info) :-
    calculate_row(Grammar, Items, STD, Row, NewSets, Status, AStatus),
    union(Rest, NewSets, WTD),
    construct_parse_table(Grammar, Items, WTD, [(STD, Row) | AM], M, AStatus, Info).
construct_parse_table(_, _, [], M, M, I, I).

calculate_row(Grammar, Items, STD, Row, NewSets, Status, Info) :-
    calculate_reduces(Grammar, STD, ARow, Status, StatusA),
    calculate_for_each_item(Grammar, Items, STD, ARow, Row, NewSets, StatusA, Info).

calculate_for_each_item(_, _, _, _, [], [], Status, Status) :- Status \= ok, !.
calculate_for_each_item(Grammar, [Item | Items], STD, ARow, Row, NewSets, Status, Info) :-
    go_to(Grammar, STD, Item, NewSet),
    (NewSet = [] ->
        calculate_for_each_item(Grammar, Items, STD, ARow, Row, NewSets, Status, Info);
        (Item = nt(_)->
            Op = goto(NewSet);
            Op = shift(NewSet)),
        (member((Item, O), ARow) ->
            calculate_for_each_item(Grammar, Items, STD, ARow, Row, NewSets, konflikt(O, Op), Info);
            calculate_for_each_item(Grammar, Items, STD, [(Item, Op) | ARow], Row, ANewSets, Status, Info)),
        NewSets = [NewSet | ANewSets]).
calculate_for_each_item(_, [], _, Row, Row, [], I, I).

calculate_reduces(_, _, _, Status, Status) :- Status \= ok, !.
calculate_reduces(Grammar, [dot_prod('Z', _, ['#']) | STD], [('#', accept) | Row],  Status, Info) :-
    calculate_reduces(Grammar, STD, Row, Status, Info), !.
calculate_reduces(Grammar, [dot_prod(E, P, []) | STD], Row, Status, Info) :-
    calculate_reduces(Grammar, STD, ARow, Status, AInfo),
    follow_set(Grammar, E, FS),
    for_all_add_reduces(reduce(nt(E), P), FS, ARow, Row, AInfo, Info).
calculate_reduces(Grammar, [dot_prod(_, _, [_|_]) | STD], Row, Status, Info) :-
    calculate_reduces(Grammar, STD, Row, Status, Info).
calculate_reduces(_, [], [], Status, Status).

for_all_add_reduces(_, _, _, _, Status, Status) :- Status \= ok, !.
for_all_add_reduces(Op, [T | FS], ARow, Row, Status, Info) :-
    (member((T, O), ARow) ->
        for_all_add_reduces(Op, FS, ARow, Row, konflikt(O, Op), Info);
        for_all_add_reduces(Op, FS, [(T, Op) | ARow], Row, Status, Info)).
for_all_add_reduces(_, [], Row, Row, Status, Status).

accept((StartSet, Table), WordN) :-
    append(WordN, ['#'], Word),
    accepts_h(Table, Word, [StartSet]).

get_action(Table, State, Letter, Action) :-
    append(_, [(State, Row) | _], Table),
    append(_, [(Letter, Action) | _], Row).

accepts_h(Table, [Letter | Word], [State | Stack]) :-
    get_action(Table, State, Letter, Op),
    perform_action(Table, [Letter | Word], Op, [State | Stack]).
perform_action(_Table, ['#'], accept, _Stack).
perform_action(Table, Word, reduce(E, P), Stack) :-
    drop_elements(P, Stack, [Q | NewStack]),
    get_action(Table, Q, E, Action),
    perform_action(Table, Word, Action, [E, Q | NewStack]).
perform_action(Table, [Letter | Word], shift(NewState), Stack) :-
    accepts_h(Table, Word, [NewState, Letter | Stack]).
perform_action(Table, Word, goto(NewState), Stack) :-
    accepts_h(Table, Word, [NewState | Stack]). 

drop_elements([_|L1], [_,_|L2], L3) :- drop_elements(L1, L2, L3).
drop_elements([], L, L).
