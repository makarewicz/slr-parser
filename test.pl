[mm306310].
grammar(ex1, [prod('E', [[nt('E'), '+', nt('T')],  [nt('T')]]),
          prod('T', [[id],  ['(', nt('E'), ')']])   ]).


grammar(ex2, [prod('A', [[nt('A'), x], [x]])]).    % LR(0)

grammar(ex3, [prod('A', [[x, nt('A')], [x]])]).    % SLR(1)

                                                   % nie SLR(1)
grammar(ex4, [prod('A', [[x, nt('B')], [nt('B'), y], []]),
              prod('B', [[]])]).


                                                   % nie SLR(1)
grammar(ex5, [prod('S', [[id], [nt('V'), ':=', nt('E')]]),
              prod('V', [[id], [id, '[', nt('E'), ']']]),
              prod('E', [[nt('V')]])]).

                                                   % nie SLR(1)
grammar(ex6, [prod('A', [[x], [nt('B'), nt('B')]]),
              prod('B', [[x]])]).

% test(+NazwaGramatyki, +ListaSlowDoZbadania)
test(NG, ListaSlow) :-
     grammar(NG, G),
     createSLR1(G, Automat, ok),
     checkWords(ListaSlow, Automat).

checkWords([], _) :-  write('Koniec testu.\n').
checkWords([S|RS], Automat) :-
      format("  Slowo: ~p ", [S]),
      (accept(Automat, S) -> true; write('NIE ')),
      write('nalezy.\n'),
      checkWords(RS, Automat).
